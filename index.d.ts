import Vue, {ComponentOptions, PluginFunction} from "vue";

/**
 * Maps lookup keys into translations of a single locale.
 */
export interface TranslationSet {
    [key: string]: string;
}

/**
 * Maps locale identifiers into sets of translations.
 */
export interface TranslationsPerLocale {
    [locale: string]: TranslationSet;
}

/**
 * Describes options supported by methods for looking up translations.
 */
export interface I18nLookupOptions {
    /**
     * ID of locale to explicitly look up instead of current one.
     */
    locale?: string;

    /**
     * ID of locale to look up if there is no translation for current or
     * explicitly selected one.
     */
    fallbackLocale?: string;

    /**
     * String value to return if looking up didn't yield any matching
     * translation.
     */
    fallback?: string;
}

/**
 * Manages i18n data in context of a component tree.
 */
declare class Translations extends Vue.Component {
    /**
     * Current locale of component tree.
     */
    locale: string;

    /**
     * Exposes translations for all supported locales.
     */
    map: TranslationsPerLocale;

    /**
     * Looks up translation for provided key in selected or current locale.
     *
     * @param key key to look up
     * @param options customizations for lookup
     * @param options.locale explicitly selected locale of desired translation, omit for current one
     * @param options.fallbackLocale ID of locale to use if there is no translation in selected/current one
     * @param options.fallback string to return if translation is missing
     */
    lookup( key: string, options: { locale?: string, fallbackLocale?: string, fallback?: string } ): string;
}

declare module "vue/types/options" {
    interface ComponentOptions<V extends Vue> {
        /**
         * Selects custom prefix to use on globally exposing widgets. Default
         * prefix is `hitchy`, thus exposing e.g. `hitchy-card` component.
         */
        prefix?: string;

        /**
         * Provides custom event bus or factory for event bus used per component
         * tree. If omitted, every component tree is using its own event bus
         * automatically.
         */
        translations?: TranslationsPerLocale;

        /**
         * Selects locale to use as initially current one in context of
         * component tree.
         */
        locale?: string;
    }
}

declare module "vue/types/vue" {
    interface Vue {
        /**
         * Exposes i18n data of component tree.
         */
        $i18n: Translations;
    }
}

declare class CepharumVueI18n {
    static install: PluginFunction<never>;
}

export default CepharumVueI18n;
