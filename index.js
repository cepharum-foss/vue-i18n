/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

/**
 * @typedef {object} I18nOptions
 * @param {object} translations per-locale tree of translations
 * @param {string} prefix prefix to use instead of `i18n` for exposing components globally
 * @param {string} locale select locale to be initially current one
 */

/**
 * Installs plugin when used with `Vue.use()`.
 *
 * @param {object} Vue instance of Vue to integrate with
 * @param {I18nOptions} options customizes support for i18n in a component tree
 * @returns {void}
 */
export function install( Vue, options ) {
	const {
		translations: initialTranslations = null,
		prefix = "i18n",
		name = null,
		directive = null,
		tag = null,
		locale: initialLocale = null,
		fallbackLocale: globalFallbackLocale = "en",
	} = options || {};

	/**
	 * Implements container for exposing reactive map of multi-language
	 * translations.
	 */
	Vue.Translations = Vue.extend( {    // eslint-disable-line no-param-reassign
		data() {
			return {
				locale: normalizeLocale( initialLocale || navigator.language ),
				fallbackLocale: globalFallbackLocale,
				map: {}
			};
		},
		methods: {
			/**
			 * Looks up translation for internationalized key or returns
			 * provided string given otherwise.
			 *
			 * @param {string} key internationalized key (which is: prefixed with @@) or some regular string to pass
			 * @param {I18nLookupOptions} lookupOptions look up customizations
			 * @returns {string} translation of provided key, optional fallback is missing translation or given regular string
			 */
			lookup( key, lookupOptions = {} ) {
				if ( key.startsWith( "@@" ) ) {
					const { locale = null, fallbackLocale = this.fallbackLocale, fallback = null, data = null } = lookupOptions;

					let normalized = normalizeLocale( locale || navigator.language );
					if ( normalized ) {
						let map = this.map[normalized];

						if ( !map ) {
							map = this.map[normalized.replace( /-.+$/, "" )];
						}

						if ( map && map.hasOwnProperty( key ) ) {
							const text = map[key];

							return data ? mustache( text, data ) : text;
						}
					}

					if ( fallbackLocale ) {
						console.warn( `missing translation for ${key} in locale ${normalized} -> trying ${fallbackLocale} as fallback` );

						normalized = normalizeLocale( fallbackLocale );
						if ( normalized ) {
							let map = this.map[normalized];

							if ( !map ) {
								map = this.map[normalized.replace( /-.+$/, "" )];
							}

							if ( map && map.hasOwnProperty( key ) ) {
								const text = map[key];

								return data ? mustache( text, data ) : text;
							}
						}
					}

					if ( fallback != null ) {
						return data ? mustache( fallback, data ) : fallback;
					}

					console.warn( `missing translation for ${key} in locale ${normalized}` );

					return "<missing translation>";
				}

				return key;
			},
		},
	} );

	/**
	 * Implements component for rendering translation for provided lookup key.
	 */
	Vue.component( tag || name || `${prefix}-lookup`, {
		props: {
			id: {
				type: String,
				required: true
			},
			fallbackLocale: String,
			fallback: String,
			html: Boolean,
			tag: String,
			data: Object,
		},
		computed: {
			translation() {
				return this.id.startsWith( "@@" ) ? this.$i18n.lookup( this.id, {
					fallbackLocale: this.fallbackLocale,
					fallback: this.fallback,
					data: this.data,
				} ) : this.id;
			},
		},
		render( h ) {
			if ( this.html ) {
				return h( this.tag || "span", {
					class: "localized",
					domProps: {
						innerHTML: sanitizeHtml( this.translation ),
					},
				} );
			}

			return h( this.tag || "span", { class: "localized" }, [this.translation] );
		},
	} );

	Vue.mixin( {
		created() {
			if ( this.$root === this ) {
				this.$i18n = new Vue.Translations( { parent: this } );

				if ( initialTranslations ) {
					this.$i18nLoadMulti( initialTranslations );
				}
			} else {
				this.$i18n = this.$root.$i18n;
			}
		},
	} );

	/**
	 * Loads translations of multiple locales.
	 *
	 * @param {object} translations per-locale tree of translations
	 * @param {boolean} replace set true for translations missing in provided tree dropped from existing map
	 * @returns {void}
	 */
	Vue.prototype.$i18nLoadMulti = function( translations, replace = false ) {  // eslint-disable-line no-param-reassign
		const locales = Object.keys( translations );
		const numLocales = locales.length;

		for ( let i = 0; i < numLocales; i++ ) {
			const locale = locales[i];

			this.$i18nLoad( locale, translations[locale], replace );
		}
	};

	/**
	 * Loads translations for provided map.
	 *
	 * @param {string} locale locale of provided translations
	 * @param {object} translations tree of translations
	 * @param {boolean} replace set true for translations missing in provided tree dropped from existing map
	 * @returns {void}
	 */
	Vue.prototype.$i18nLoad = function( locale, translations, replace = false ) {   // eslint-disable-line no-param-reassign
		const normalized = normalizeLocale( locale );
		if ( !normalized ) {
			throw new TypeError( `invalid locale: ${locale}` );
		}

		const commonLanguage = normalized.replace( /-.+/, "" );
		const map = this.$i18n.map;

		if ( !map[normalized] ) {
			Vue.set( map, normalized, {} );
		}

		if ( !map[commonLanguage] ) {
			Vue.set( map, commonLanguage, {} );
		}

		const hasCommon = commonLanguage !== normalized;
		const used = loadingLevel( map, translations, normalized, hasCommon ? commonLanguage : null );

		if ( replace ) {
			let target = map[normalized];

			for ( const key in Object.keys( target ) ) {
				if ( !used[key] ) {
					Vue.delete( target, key );
				}
			}

			if ( hasCommon ) {
				target = map[commonLanguage];

				for ( const key in Object.keys( target ) ) {
					if ( !used[key] ) {
						Vue.delete( target, key );
					}
				}
			}
		}

		/**
		 * Injects another hierarchy of provided tree of translations into
		 * current component.
		 *
		 * @param {object} target reactive map of translations per locale
		 * @param {object} translationsLevel tree of translation to inject
		 * @param {string} specific locale of translations provided explicitly
		 * @param {?string} common additional locale super-ordinated to explicitly provided one
		 * @param {string} _prefix prefix to use for current level of translations in resulting map
		 * @returns {object<string, boolean>} tracks injected translations
		 */
		function loadingLevel( target, translationsLevel, specific, common, _prefix = "@@" ) {
			const names = Object.keys( translationsLevel );
			const numNames = names.length;

			for ( let i = 0; i < numNames; i++ ) {
				const key = names[i];
				const value = translationsLevel[key];

				switch ( typeof value ) {
					case "string" :
						Vue.set( target[specific], `${_prefix}${key}`, value );

						if ( common ) {
							Vue.set( target[common], `${_prefix}${key}`, value );
						}
						break;

					case "object" :
						if ( value ) {
							loadingLevel( target, value, specific, common, `${_prefix}${key}_` );
						}
						break;
				}
			}
		}
	};

	if ( Vue.prototype._ == null ) {
		/**
		 * Aliases i18n lookup using name equivalent to gettext.
		 *
		 * @param {string} key string to pass or some internationalized key to look up related translation in current locale
		 * @param {I18nLookupOptions} lookupOptions lookup customizations
		 * @returns {string} found lookup, provided string on lookup failure
		 */
		Vue.prototype._ = function( key, lookupOptions ) {  // eslint-disable-line no-param-reassign
			return this.$i18n.lookup( key, lookupOptions );
		};
	}

	Vue.directive( directive || name || `${prefix}-lookup`, function( el, { value, oldValue, modifiers, arg }, vnode ) {
		if ( value !== oldValue ) {
			const component = vnode.componentInstance;

			el.classList.add( "translated" );

			let text = ( component || vnode.context ).$i18n.lookup( value );
			let prop = "innerText";
			let native = true;

			if ( modifiers.html ) {
				text = sanitizeHtml( text );
				prop = "innerHTML";
			} else {
				switch ( arg ) {
					case "title" :
					case "placeholder" :
					case "value" :
						prop = arg;
						native = false;
				}
			}

			if ( !native && component && ( prop in component ) ) {
				component[prop] = text;
				return;
			}

			el[prop] = text; // eslint-disable-line no-param-reassign
		}
	} );
}

/**
 * Normalizes provided locale or current locale of browser.
 *
 * @param {string} locale locale to normalize instead of browser's current one
 * @returns {?string} normalized locale, null if there is no valid locale
 */
export function normalizeLocale( locale ) {
	const match = /^([a-z]{2,3})(?:[-_]([a-z]{2,3}))?(?:@(.+))?$/i.exec( locale );

	if ( match ) {
		const language = match[1].toLowerCase();
		const country = ( match[2] || "" ).toUpperCase();

		return country ? `${language}-${country}` : language;
	}

	return null;
}

/**
 * Disables dangerous HTML markup in provided string.
 *
 * @param {string} code text optionally containing some HTML markup
 * @returns {string} provided text with dangerous markup disabled
 */
export function sanitizeHtml( code ) {
	return code
		.replace( /<(\/?\s*(?:script|style|object|iframe|link|audio|video|\w*-))/ig, "&lt;$1" )
		.replace( /(<[^>]+\b)(href|src)=("|'|)\s*(\w+):.*?\3/ig, ( all, pre, name, quote, scheme ) => {
			switch ( scheme.toLowerCase() ) {
				case "http" :
				case "https" :
				case "mailto" :
					return all;

				default :
					return pre + " ";
			}
		} );
}

/**
 * Replaces occurrences of double pairs of curly braces with property of given
 * data selected by name wrapped in those curly braces.
 *
 * @param {string} template some string to contain double pairs of curly braces (placeholders)
 * @param {object} data set of named properties available for addressing in placeholders
 * @returns {string} provided string with occurrences of placeholders replaced
 */
export function mustache( template, data ) {
	return String( template ).replace( /{{([^}]+)}}/g, ( _, name ) => {
		const trimmed = name.trim();

		if ( data.hasOwnProperty( trimmed ) ) {
			return data[trimmed];
		}

		const lowercase = name.toLowerCase();

		if ( data.hasOwnProperty( lowercase ) ) {
			return data[lowercase];
		}

		return "";
	} );
}

export default { install };

